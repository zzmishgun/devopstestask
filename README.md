# README

## Project Overview

This project sets up an OpenResty and Netdata environment using Docker Compose. The configuration files and deployment scripts are designed to automate the deployment process using GitLab CI and Ansible.

## File Descriptions

### 1. `.gitlab-ci.yml`
This file is the GitLab CI configuration that defines the pipeline for deploying the services.

- **Stages:** Defines the deployment stage.
- **Deploy Job:**
  - Installs Ansible.
  - Runs the Ansible playbook to deploy the services.
  - Triggered only on the `main` branch.

### 2. `docker-compose.yml`
This Docker Compose configuration defines two services: OpenResty and Netdata.

- **OpenResty:**
  - Uses the `openresty/openresty` image.
  - Exposes port 8080.
  - Mounts the custom `nginx.conf` file.
- **Netdata:**
  - Uses the `netdata/netdata` image.
  - Exposes port 29999.
  - Includes additional capabilities for debugging.

### 3. `inventory.ini`
This Ansible inventory file lists the servers where the playbook will be executed.

- Defines `localhost` with a local connection.

### 4. `nginx.conf`
This Nginx configuration file for OpenResty sets up a basic HTTP server that proxies requests to the Netdata service.

- Configures worker processes and connections.
- Sets up a server to listen on port 80.
- Proxies requests to Netdata running on port 19999.

### 5. `playbook.yml`
This Ansible playbook automates the deployment of the Docker Compose setup.

- **Tasks:**
  - Ensures `docker-compose` is installed.
  - Copies the `docker-compose.yml` and `nginx.conf` files to the server.
  - Runs `docker-compose up` to start the services.

## Deployment Instructions

1. **Ensure GitLab Runner is configured**:
   - Make sure you have a GitLab Runner configured and available to run the CI jobs.

2. **Commit and push changes to the `main` branch**:
   - The CI/CD pipeline is triggered only on the `main` branch. Ensure all changes are committed and pushed to `main`.

3. **Pipeline Execution**:
   - The GitLab CI pipeline will automatically run the deploy job, installing Ansible and executing the Ansible playbook to deploy the services.

4. **Access the services**:
   - OpenResty: Accessible at `http://<server-ip>:8080`
   - Netdata: Accessible at `http://<server-ip>:29999`


## Acknowledgments

- [OpenResty](https://openresty.org/)
- [Netdata](https://www.netdata.cloud/)
- [Ansible](https://www.ansible.com/)
- [Docker](https://www.docker.com/)


---
